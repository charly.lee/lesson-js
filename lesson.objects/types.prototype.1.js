var arr1 = [1, 2, 3];

console.log(arr1);

console.log(Array.prototype === arr1.__proto__);
console.log(Object.prototype === arr1.__proto__.__proto__);
console.log(null === arr1.__proto__.__proto__.__proto__);

console.log(Array.prototype.push);

Array.prototype.push.apply(arr1, [4, 5]);
console.log(arr1);

Array.prototype.push.call(arr1, 6, 7, 8);
console.log(arr1);

var fn1 = function() {
  console.log(this.length);
};

fn1.apply(arr1);
fn1.apply('1234567890');

var str2 = String.prototype.toString.apply('str');
console.log(str2);

var str1 = Array.prototype.toString.apply('str');
console.log(str1);
