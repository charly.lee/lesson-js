const log = console.log.bind(console);

function a(p1) {
  log(this);
  log(p1 + 100);
  console.log(p1 + 1000);
}

a(1);
