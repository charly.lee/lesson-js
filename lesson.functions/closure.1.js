function fn1() {
  var a = 0;

  return {
    getA2: function() {
      return a + 1;
    },
    setA2: function(p) {
      a = p;
    }
  };
}

var v = fn1();

console.log(v);
console.log(v.setA2(100));
console.log(v.getA2());

var v2 = fn1();
console.log(v2);
console.log(v2.setA2(1000));
console.log(v2.getA2());
