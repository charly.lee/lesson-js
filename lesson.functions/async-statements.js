function a() {
  let a = 1;
  a = 2;

  fetch("/a").done(function (data) {
    a = 3;
    console.log(a); // 3
  });

  console.log(a); // 2
}
