function User(n) {
  this.name = n;

  this.fn1 = function() {
    return this.name;
  };
}

var u1 = new User('Charly');
var u2 = new User('Tim');

console.log(u1);

console.log(u1.name);
console.log(u2.name);

console.log(u1.fn1());
console.log(u1.fn1.apply(u2));

/* in Java
public class User {
  public String name = "";
}
*/
