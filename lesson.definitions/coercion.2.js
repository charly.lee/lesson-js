const log = console.log.bind(console);

var n = 4;
log(n, typeof n);

var s = '3';
log(s, typeof s);

log(s + n, typeof (s + n));
log(n + s, typeof (n + s));

log(Number.parseInt('34'), typeof Number.parseInt('34'));
