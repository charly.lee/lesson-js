const log = console.log.bind(console);

var n = 1.1;
var s = "Aa";
var b = true;

log(typeof n);
log(typeof s);
log(typeof b);

var nil = null;
var u;

log(typeof nil);
log(typeof u);

var o = {
  name: "Charly",
  age: 30,
};

log(typeof o);

var d = new Date();
log(typeof d);

var r = /dsfsd/;
log(typeof r);

log(d.getMonth());

var a = [1, 2, 3, "a", o];
log(a[4]);

// Array.prototype.sort() : prototype functions
var a2 = [3, 2, 1];
log(a2.sort());

// Math.abs(x) : static functions
log(Math.abs(3.1));

var o2 = {
  a: 0,
  fn1: function () {
    return 0;
  },
};
