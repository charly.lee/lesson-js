var value = 1;

function bar() {
  console.log(this);
  console.log(this.value);
}

var obj = {
  value: 10,
  foo: function () {
    console.log(this.value);
    bar.apply(this);
  },
};

obj.foo();

console.log(global);

console.log(this === global); // false, node
