if (a) {
  console.log("if(a) => meets" + b);
}

console.log(a + b);
console.log(a + c, typeof (a + c));
console.log(a + d);
console.log(a + e);
console.log(a + h);
console.log(a + i);

var a = 1;
var b = 0;
var c = "A";
var d = "";
var e = true;
var f = false;
var g = {};
var h = { a: 1 };
var i = "1";
