# Javascript Browser-Node Cross code.

## Javascript Runtime

- Browser window
- Node.js

## Global context:

### Node.js

```js
console.log(global); // Object [global]
console.log(this); // Object [global]
console.log(global === this); // true

global.test_1 = function () {
  console.log(this); // Object [global]
};

global.test_1();
```

### Browser window

```html
<html>
  <body>
    <script>
      // console.log(global);
      // Uncaught ReferenceError: global is not defined

      console.log(window); // Object [Window]
      console.log(this); // Object [Window]
      console.log(window === this); // true

      function test_1() {
        console.log(this); // Object [Window]
      }

      test_1();
    </script>
  </body>
</html>
```

## Global object for both runtimes:

```js
if (typeof global === "undefined") {
  global = {
    window,
  };
} else {
  // node.js
  global.window = {};
  module.exports = global;
}
```

### Now you write same global context (or object) in both runtimes.

```js
require("./global");

global.test_1 = function () {
  console.log(this.v); // 1.0
};

global.v = "1.0";
global.test_1();
```

```html
<html>
  <head>
    <script src="./global.js"></script>
  </head>
  <body>
    <script>
      global.test_1 = function () {
        console.log(this.v); // 1.0
      };

      global.v = "1.0";
      global.test_1();
    </script>
  </body>
</html>
```

## Next

### Looking-up runtime

```js
const isInBrowser = typeof window === "undefined" ? false : true;
const isNodeProcess = !isInBrowser;
```

### Javascript Modular

- Node.js commonjs style
- ES6 modular style
- Cross Platform Modular
