Javascript Cheat-Sheet

Basic
- Coercion
- Hoisting
- Boolean, Number, String, Date
- Array

Function
- Function Context
- Bind, call, apply
- Scope, closure

Object
- Properties
- Create, clone, extend

Prototype
- Prototype and object
- Prototype chain, inherit, override

Promise
- Promise, then
- Promise chaining
- Promise error handling
- Async & Await (ES7)

Others
- Regex
- JQuery
- Lodash
- Underscore

